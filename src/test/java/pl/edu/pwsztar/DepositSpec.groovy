package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    @Unroll
    def "Deposit money on account number #accountNumber"() {
        given: "Initial data"
            def bank = new Bank();
            bank.createAccount();
            bank.createAccount();
            bank.createAccount();
        when: "despoit money on account"
            int exist = bank.deposit(accountNumber ,amount)
        then:
            exist== check
        where:
            accountNumber | amount  || check
            1             | 200     || true
            2             | 30      || true
            3             | 160     || false
    }
}
