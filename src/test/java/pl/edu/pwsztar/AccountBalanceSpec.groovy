package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "Check money on account #accountNumber"(){
        given: "Initial data"
        def bank = new Bank()
        bank.createAccount()
        bank.createAccount()
        bank.createAccount()
        bank.createAccount()
        when: "Deposit money on account"
        def balance = bank.accountBalance(accountNumber)
        then:
        balance == money
        where:
        accountNumber | money
        1             | 50
        2             | 1000
        3             | 200
    }
}
