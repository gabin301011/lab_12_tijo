package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountBalanceSpec extends Specification{
    @Unroll
    def "return sum of accounts balance" (){
        given: "initial data"
        def bank = new Bank()
        bank.createAccount()
        bank.deposit(1,100)
        bank.createAccount()
        bank.deposit(2,200)
        when: "expect to get sum of all accounts balance"
        int totalSum = bank.sumAccountsBalance()
        then: "expect to get sum balance"
        totalSum == 300
    }
}
