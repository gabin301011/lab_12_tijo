package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should delete account number for #accountNumber and return balance" () {
        given: "initial data"
            def bank = new Bank();
            bank.createAccount();
            bank.createAccount();
            bank.createAccount();
            bank.createAccount();
        when: "the account is deleted"
            int num = bank.deleteAccount(accountNumber)
        then: "check account balance"
            num == numberAfterDelete
        where:
            accountNumber ||  numberAfterDelete
            1             ||  0
            2             ||  0
            3             ||  0
    }
}
