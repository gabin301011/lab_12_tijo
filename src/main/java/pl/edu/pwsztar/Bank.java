package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;
    private List<Account> listAccounts = new ArrayList<>();

    public int createAccount() {
        listAccounts.add(new Account(++accountNumber,0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int balance = accountBalance(accountNumber);
        if(balance == ACCOUNT_NOT_EXISTS){
            System.out.println("Account not exists");
        }else{
            listAccounts.remove(accountNumber);
        }
        return balance;
     }

    public boolean deposit(int accountNumber, int amount) {
        Account acc = finder(accountNumber).orElse( new Account(ACCOUNT_NOT_EXISTS,0));

        if(acc.getAccountNumber() != ACCOUNT_NOT_EXISTS && amount > 0){
            acc.setBalance(acc.getBalance() + amount);
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        return false;
    }

    public int accountBalance(int accountNumber) {
        Account acc = finder(accountNumber).orElse( new Account(ACCOUNT_NOT_EXISTS,0));

        if(acc.getAccountNumber() == ACCOUNT_NOT_EXISTS) {
            return ACCOUNT_NOT_EXISTS;
        }
        else {
            return acc.getBalance();
    }

}

    public int sumAccountsBalance() {
        int totalSum = 0;
        for(Account account : listAccounts){
            totalSum += account.getBalance();
        }
        return totalSum;
    }

    private Optional<Account> finder(int accountNumber){
        for(Account account : listAccounts){
            if(account.getAccountNumber() == accountNumber){
                return Optional.of(account);
            }
        }
        return Optional.empty();
    }

}
